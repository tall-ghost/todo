package com.example.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.backend.models.Tache;

public interface TacheRepository extends JpaRepository<Tache, Long> {

}