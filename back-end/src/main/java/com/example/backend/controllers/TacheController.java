package com.example.backend.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.backend.models.Tache;
import com.example.backend.repositories.TacheRepository;

@RestController
@RequestMapping("/todo/")
@CrossOrigin(origins = "http://localhost:4200")
public class TacheController {
    @Autowired
    private TacheRepository tacheRepo;

    @GetMapping("/taches")
    public List<Tache> getAllTaches() {
        return tacheRepo.findAll();
    }
}