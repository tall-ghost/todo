export class Tache {
  id?: number;
  nom?: string;
  description?: string;
  debut?: Date;
  fin?: Date;
}
