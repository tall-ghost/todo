import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  private server = "http://localhost:8080/todo/";
  constructor(private http: HttpClient) { }

  getAllTaches() : Observable<any>{
    return this.http.get(this.server + 'taches');
  }
}
