import { Component, OnInit } from '@angular/core';
import { Tache } from 'src/app/models/Tache';
import { ServicesService } from 'src/app/services/services.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  constructor(private service: ServicesService) {}

  taches!: Tache[];

  ngOnInit(): void {}

  getAllTaches() {
    this.service.getAllTaches().subscribe(
      (data) => {
        this.taches = data;
      },
      (error) => console.log(error)
    );
    console.log(this.taches);
  }
}
